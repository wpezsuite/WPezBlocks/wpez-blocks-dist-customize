<?php
/*
Plugin Name: WPezBlocks: Experimental Customize
Plugin URI: https://gitlab.com/wpezsuite/WPezBlocks/wpez-blocks-dist-customize
Description: The proving grounds for WPezBlocks
Version: 0.0.0.1
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: http://AlchemyUnited.com
License: GPLv2+
Text Domain: wpez-blocks
*/

namespace WPezBlocksExpCust;

// No WP? Die! Now!!
if (!defined('ABSPATH')) {
    header('HTTP/1.0 403 Forbidden');
    die();
}

// if ( ! class_exists())

class Customize
{

    protected $_str_plugin_url;
    protected $_str_path_slug;
    protected $_str_style_ver;
    protected $_str_script_handle_primary;


    public function __construct($arr_args = false)
    {

        $this->setPropertydefaults($arr_args);

    }

    protected function setPropertydefaults($arr_args)
    {

        $arr_defs = [
            'plugin_url' => plugins_url(null, __FILE__),
            'script_handle_primary' => 'wpezblocks-js',
            'assets_path_slug' => '/App/assets/dist/css/scss',
            'style_ver' => '0.0.5t'
        ];

        if (is_array($arr_args)) {

            $arr_args = array_merge($arr_defs, $arr_args);

        }


        $this->_str_plugin_url = $arr_args['plugin_url'];
        $this->_str_path_slug = $arr_args['assets_path_slug'];
        $this->_str_script_handle_primary = $arr_args['script_handle_primary'];
        $this->_str_style_ver = $arr_args['style_ver'];

        // $this->_str_plugin_dir = $arr_args['plugin_dir'];

    }


    public function enqueueBlockFrontendAssets()
    {

        wp_enqueue_style(
            'wpez-blocks-customize-frontend-css',
            $this->_str_plugin_url . $this->_str_path_slug . '/blocks.frontend.css',
            ['wpez-blocks-css'],
            $this->_str_style_ver,
            'all'
        );
    }

    public function enqueueBlockEditorAssets()
    {

        wp_enqueue_style(
            'wpez-blocks-customize-editor-css',
            $this->_str_plugin_url . $this->_str_path_slug . '/blocks.editor.css',
            [],
            $this->_str_style_ver,
            'all'
        );
    }


    public function scriptsLocalize()
    {


        wp_localize_script($this->_str_script_handle_primary, 'wpezBlocksTextHeading', $this->textHeading());

    }

    public function animateOnScroll()
    {

        // https://github.com/michalsnik/aos/blob/master/src/js/aos.js
        $arr = [
            //   'active' => true,
            //   'offset' => 100,

            // what px width (or less) will aos be disabled?
            //   'disable' => 1000
        ];

        wp_localize_script('wpezblocks-front-js', 'wpezBlocksAOS', $arr);


    }

    protected function textHeading()
    {

        $arr = [
            // index.js
            'register__TextHeading' => [
                'active' => true,
            ],

            // TextHeading.js
            'block__TextHeading' => [
                'initialize__attributes' => [
//                 'featureSet' => '50',   // 50 = extra
//                 'blockWidth' => 'full',
//                 'blockAnchor' => 'my-test-anchor-123',
                ],
            ],

            //
            'edit__RichTextSingle' => [
                'placeholder' => 'My custom placeholder',
//                'formattingControls' => ['bold', 'italic'],
//                'keepPlaceholderOnFocus' => true,
//              'active__AttributeFeatureSet' => false,
                'active__PanelBodyWrapper' => true,
                'active__PanelBodyRichTextSingleCustomize' => true,
//                'active__PanelBodyColorCombos' => false,

                'attribute__featureSet' => [
//                    'exclude' => ['01'],
//                    'disabled' => ['99'],
                ]

            ],

            'inspector__PanelBodyWrapper' => [

//                'panelBodyTitle' => 'Wrapper args',
//                'initialOpen' => true,


                // block
                'attribute__blockWidth' => [
//                    'active' => false,
//                'disabled' => ['full'],
//                    'exclude' => ['content']
                ],

                'attribute__blockHeight' => [
//                    'active' => false,
//                    'exclude' => [], // 'xxs', 'xs', 'sm', 'md, 'lg', 'xl', 'xxl'
                ],

                'attribute__blockAnchor' => [
//                    'active' => false,
                ],

                // box
                'attribute__boxWidth' => [
//                    'active' => false,
                ],

                'attribute__boxHeight' => [
//                    'active' => false,
                ],

                'attribute__boxAlign' => [
//                    'active' => false,
                ],

                'attribute__boxAlignVert' => [
//                    'active' => false,
                ],

                // block - animate
                'attribute__blockAniName' => [
//                    'active' => false,
                ],

                'attribute__blockAniDuration' => [
//                    'active' => false,
                ],
                'attribute__blockAniDelay' => [
//                    'active' => false,
                ],
                'attribute__blockAniIterations' => [
//                    'active' => false,
                ],

//                'attribute__blockAniEasing' => [
//                    'active' => false,
//                ],

                'attribute__blockAniEleAnchor' => [
//                    'active' => false,
                ],

                'attribute__blockAniWinAnchor' => [
//                   'active' => false,
                ],
                'attribute__blockAniOffset' => [
//                    'active' => false,
                ],


                // box - animate
                'attribute__boxAniName' => [
//                    'active' => false,
                ],

                'attribute__boxAniDuration' => [
//                    'active' => false,
                ],

                'attribute__boxAniDelay' => [
//                    'active' => false,
                ],
                'attribute__boxAniIterations' => [
//                   'active' => false,
//                   'disabled' => ['2', '4', '6', 'loop']
                ],

//                'attribute__boxAniEasing' => [
//                    'active' => false,
//                ],

                'attribute__boxAniEleAnchor' => [
                    // 'active' => false,
                    'defaultValue' => 'top'
                ],

                'attribute__boxAniWinAnchor' => [
//                  'active' => false,
                    'defaultValue' => 'bottom'
                ],
                'attribute__boxAniOffset' => [
//                    'active' => false,
                ],


            ],

            'inspector__PanelBodyRichTextCustomize' => [
                //       'panelBodyTitle' => 'Customize',
                //       'initialOpen' => false,

                'attribute__textTagName' => [
//                   'active' => false,
//                    'label' => '',
//                    'labelAria' => '',
//                    'tooltipPosition' => 'top center',
//                    'exclude' => ['h6'],
//                   'disabled' => ['h1'],
//                    'defaultKey' => 'h2',
//                    'resetTooltip' => 'Reset to H2',
                ],
            ],

            'inspector__PanelBodyColorCombos' => [
//                  'panelBodyTitle' => 'Colorzzzzzz',
//               'initialOpen' => true,

                'attribute__colorComboBasic' => [
                    //                    'active' => false,
                    // 'label' => 'Select Color Combo',
                    // 'ariaLabel' => 'yyy Select Color Combo',
                    // 'defaultKey' =>
                    // 'help' => '',
                    // 'rowClassName' =>
//                     'exclude' => ['combo-0'],
                    // 'disabled' => []
                    // 'classField' =>
                    // 'columns' => '1',
                    // 'orientation' => 'landscape',
                    // 'buttonTextActive' => false,
                    // 'tooltipActive' => true,
                    // 'tooltipPosition' => 'top center',
                    // 'resetActive' => true,
                    // 'resetButtonText' => 'Reset',
                    // 'resetTooltip' => 'Reset to Default',
                    // 'resetTooltipPosition' => 'top left',


// remove the X_ to see these button definitions in action
                    'X_buttons' => [
                        [
                            'buttonText' => 'Zero Zero',
                            'buttonTextActive' => true,
                            'ariaLabel' => 'Combo: Green + Red',
                            'tooltipText' => 'Zero tooltipText',
                            // tooltipPosition => '',
                            'key' => 'combo-0',
                            'value' => 'combo-0',
                            'combo' => ['#007445', '#FF0000'],
                            // default => true,
                            // disabled => true,
                            // className => ''

                        ],
                        [
                            'buttonText' => 'One One',
                            'buttonTextActive' => false,
                            'ariaLabel' => 'Combo: Blue + Purple',
                            'tooltipText' => 'One',
                            'key' => 'combo-1',
                            'value' => 'combo-1',
                            'combo' => ['blue', 'purple'],

                        ]

                    ]
                ],


            ],


        ];


        return $arr;


    }


}

$arr_args = [
    'plugin_url' => plugins_url(null, __FILE__),
    'script_handle_primary' => 'wpezblocks-js'
];

$new = new Customize($arr_args);

add_action('wp_enqueue_scripts', [$new, 'enqueueBlockFrontendAssets'], 100);
add_action('enqueue_block_editor_assets', [$new, 'enqueueBlockEditorAssets'], 100);

add_action('enqueue_block_editor_assets', [$new, 'scriptsLocalize'], 50);
add_action('wp_enqueue_scripts', [$new, 'animateOnScroll'], 50);





